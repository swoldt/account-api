# Form3 Accounts HttpClient in Golang
This client library provides an interface to access [form3/account-api](http://api-docs.form3.tech/api.html#organisation-accounts).

## Cody style/organization and philosophy

[Standard Package Layout](https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1)

[Go best practices, six years in](https://peter.bourgon.org/go-best-practices-2016/)

## Prequisites

* Install docker and docker-compose

* Clone this repository

## Run the project:

    docker-compose up

## Interfaces
Two interfaces are defined for this library

* __API__ Implements the following interface methods `create`, `delete`, `fetch`, and `list` (see pkg/interfaces/api.go)

* __Client__ Is a wrapper implementation around the http std lib handling request/response between the API service and the accountapi container (see pkg/interfaces/client.go)

## Domain
* `account` struct properties are annotated with validation logic 
(see pkg/domain/account.go).

* Validation is provided by [github.com/go-playground/validator/v10](https://github.com/go-playground/validator/v10).

* Validation requirements are described in [form3/account-api-docs](https://api-docs.form3.tech/api.html#organisation-accounts)

## Test (coverage: 80%)

* Tests are run within the accounts-api-test container as described in docker-compose.yaml.

* Code is [testified](https://github.com/stretchr/testify/) and http client has been mocked with [gock](https://github.com/h2non/gock)


* Auto-reload is provided through [fswatch](https://github.com/emcrisostomo/fswatch) (reference in resources/dev.Dockerfile)

* accounts-api-test depends on `accountapi`, `postgresql` and `vault` 

