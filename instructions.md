## Cody style/organization and philosophy

[Standard Package Layout](https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1)

[Go best practices, six years in](https://peter.bourgon.org/go-best-practices-2016/)

## Prequisites

Install golang

Setup appropriate goPath/goRoot/goBin.

Clone this repository into your appropriate go path

eg:

`~/go/src/bitbucket.org/swoldt/accounts-api`

Install dep: go get -u github.com/golang/dep/...

Run dep ensure // ONLY IF VENDOR FOLDER DOES NOT EXIST

See Readme instructions in accounts-api/resources/db to setup a dev db

## Run the project:

    docker-compose up

in case no migration is applied

    cd ./resources/db/ && ./create_migration.sh

## Run tests only:

    docker-compose up timescale-docker &
    make test
    docker-compose down

## Configurable Properties

The app configuration file can be found in accounts-api/config/dev.toml

    apiPort: port the backend will be exposed on
    db.url: host of the database
    db.user: database username
    db.password: database password
    db.db_name: database name
    db.ssl_mode: secure connection ("enable"/"disable")

## Deploying

- Build the docker image `make image`
- Push the image to the registry `make push`
- Push the configuration to kubernetes `make todo-config`; if updating configuration,
  then delete the existing config with `kubectl delete config todo-config`
- Deploy the new image to kubernetes `make deployment`
- (Only for the first time): create the service `kubectl apply -f service.yaml`
- (Only for the first time): create the ingress `kubectl apply -f ingress.yaml`
# Form3 Take Home Exercise

## Instructions
This exercise has been designed to be completed in 4-8 hours. The goal of this exercise is to write a client library 
in Go to access our fake [account API](http://api-docs.form3.tech/api.html#organisation-accounts) service. 

### Should
- Client library should be written in Go
- Document your technical decisions
- Implement the `Create`, `Fetch`, `List` and `Delete` operations on the `accounts` resource. Note that filtering of the List operation is not required, but you should support paging
- Ensure your solution is well tested to the level you would expect in a commercial environment. Make sure your tests are easy to read.
- If you encounter any problems running the fake accountapi we would encourage you to do some debugging first, 
before reaching out for help

#### Docker-compose
 - Add your solution to the provided docker-compose file
 - We should be able to run `docker-compose up` and see your tests run against the provided account API service 

### Please don't
- Use a code generator to write the client library
- Use a library for your client (e.g: go-resty). Only test libraries are allowed.
- Implement an authentication scheme

## How to submit your exercise
- Include your name in the README. If you are new to Go, please also mention this in the README so that we can consider this when reviewing your exercise
- Create a private [GitHub](https://help.github.com/en/articles/create-a-repo) repository, copy the `docker-compose` from this repository
- [Invite](https://help.github.com/en/articles/inviting-collaborators-to-a-personal-repository) @form3tech-interviewer-1 to your private repo
- Let us know you've completed the exercise using the link provided at the bottom of the email from our recruitment team

## License
Copyright 2019-2020 Form3 Financial Cloud

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
