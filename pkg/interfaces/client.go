package interfaces

import "io"

// APIClient interface definition
type APIClient interface {
	Do(method, path string, params io.Reader, result interface{}, expectedStatus int) error
}
