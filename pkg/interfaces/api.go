package interfaces

import "bitbucket.org/swoldt/accounts-api/pkg/domain"

// API interface definition
type API interface {
	Create(account domain.Account) (*domain.Account, error)
	Fetch(accountID string) (*domain.Account, error)
	List(params *domain.ListAccountParams) ([]domain.Account, error)
	Delete(accountID string, version int) error
}
