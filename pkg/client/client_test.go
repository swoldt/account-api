package client_test

import (
	"github.com/stretchr/testify/require"
	"gopkg.in/h2non/gock.v1"
)

func (s *TestSuite) Test_Client() {

	expected := map[string]string{"foo": "bar"}
	expectedStatus := 200
	wrongStatus := 400
	expectedPath := "/bar"

	gock.New(s.form3AccountsURL).
		Get(expectedPath).
		Reply(expectedStatus).
		JSON(expected)

	result := map[string]string{}

	err := s.client.Do("GET", expectedPath, nil, &result, expectedStatus)
	require.NoError(s.T(), err)
	require.Equal(s.T(), expected, result)

	err = s.client.Do("GET", expectedPath, nil, &result, wrongStatus)
	require.Error(s.T(), err)

	// Verify that we don't have pending mocks
	require.Equal(s.T(), gock.IsDone(), true)
}
