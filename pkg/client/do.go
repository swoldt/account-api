package client

import (
	"fmt"
	"io"
	"net/http"

	"bitbucket.org/swoldt/accounts-api/pkg/domain"
	"github.com/pkg/errors"
)

// Do sends the request to the specified rest path and unmarshals the response into the
// desired results interface{} if not provided as null and under the condition that
// the received status response is as expected
func (cli *APIClient) Do(method, path string, params io.Reader, result interface{}, expectedStatus int) error {

	url := fmt.Sprintf("%s/%s", cli.config.Form3AccountsURL, path)

	req, err := cli.assembleRequest(method, url, params)
	if err != nil {
		return err
	}

	res, err := new(http.Client).Do(req)
	if err != nil {
		return errors.Wrap(err, "request failed")
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case expectedStatus:
	default:
		expected := &domain.ResponseError{}
		err = cli.readResponse(res.Body, expected)
		if err != nil {
			return errors.Wrap(err, "reading error")
		}

		return errors.New(expected.Error)
	}

	// 	check in case we are not interested in the response body
	if result != nil {
		return cli.readResponse(res.Body, result)
	}

	return nil
}
