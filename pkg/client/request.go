package client

import (
	"io"
	"net/http"

	"github.com/pkg/errors"
)

// assembleRequest() returns a pointer to a http request instance
// with method, url and params (if method type post) as inputs
func (cli *APIClient) assembleRequest(method, url string, params io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, url, params)
	if err != nil {
		return nil, errors.Wrapf(err, "%s request initialization failed for %s", method, url)
	}

	format := "application/json"
	req.Header.Add("Accept", format)
	req.Header.Add("Content-Type", format)

	return req, nil
}
