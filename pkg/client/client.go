package client

import (
	"bitbucket.org/swoldt/accounts-api/pkg/config"
	"bitbucket.org/swoldt/accounts-api/pkg/interfaces"
	"bitbucket.org/swoldt/pkg/xlogger"
)

// APIClient defines the class implementation for this package
type APIClient struct {
	config *config.ServiceConfig
	log    *xlogger.Logger
}

// New returns an initiliazed API client
func New(log *xlogger.Logger, cfg *config.ServiceConfig) interfaces.APIClient {

	return &APIClient{
		log:    log,
		config: cfg,
	}
}
