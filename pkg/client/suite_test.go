package client_test

import (
	"testing"

	"bitbucket.org/swoldt/accounts-api/pkg/factory"
	"bitbucket.org/swoldt/accounts-api/pkg/helper"
	"bitbucket.org/swoldt/accounts-api/pkg/interfaces"
	"bitbucket.org/swoldt/pkg/xlogger"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

type TestSuite struct {
	suite.Suite
	logger           *xlogger.Logger
	helper           *helper.Helper
	factory          *factory.Factory
	client           interfaces.APIClient
	form3AccountsURL string
}

func (s *TestSuite) SetupSuite() {
	s.logger = factory.DefaultLogger(&s.Suite)
	s.helper = helper.NewHelper(&s.Suite, s.logger)
	s.factory = factory.NewFactory(&s.Suite, s.logger, s.helper)
	s.client = s.init()
}

func (s *TestSuite) init() interfaces.APIClient {
	cfg, err := s.helper.GetConfig(s.helper.BytesFromFile("../../config/dev.toml"))
	require.NoError(s.T(), err)

	s.form3AccountsURL = cfg.Form3AccountsURL

	return s.factory.InitializedClient(cfg)
}

func TestRunner(t *testing.T) {
	suite.Run(t, new(TestSuite))
}
