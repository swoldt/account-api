package factory

import (
	"bitbucket.org/swoldt/accounts-api/pkg/api"
	"bitbucket.org/swoldt/accounts-api/pkg/config"
	"bitbucket.org/swoldt/accounts-api/pkg/interfaces"
)

// InitializedAccountAPI returns the API interface to the test suite
func (f *Factory) InitializedAccountAPI(cfg *config.ServiceConfig) interfaces.API {
	return api.New(f.logger, cfg)
}
