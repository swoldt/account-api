package factory

import (
	"bitbucket.org/swoldt/accounts-api/pkg/client"
	"bitbucket.org/swoldt/accounts-api/pkg/config"
	"bitbucket.org/swoldt/accounts-api/pkg/interfaces"
)

// InitializedClient returns the API interface to the test suite
func (f *Factory) InitializedClient(cfg *config.ServiceConfig) interfaces.APIClient {
	return client.New(f.logger, cfg)
}
