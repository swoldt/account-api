package factory

import (
	"bitbucket.org/swoldt/accounts-api/pkg/helper"
	"bitbucket.org/swoldt/pkg/xlogger"
	"github.com/stretchr/testify/suite"
)

// Factory represents the struct for this package
type Factory struct {
	suite  *suite.Suite
	logger *xlogger.Logger
	helper *helper.Helper
}

// NewFactory returns a factory to support the test suite
func NewFactory(s *suite.Suite, logger *xlogger.Logger, helper *helper.Helper) *Factory {
	return &Factory{suite: s, logger: logger, helper: helper}
}
