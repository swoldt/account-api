package helper

import (
	"bitbucket.org/swoldt/accounts-api/pkg/config"
	"github.com/pelletier/go-toml"
)

func (h *Helper) GetConfig(configBytes []byte) (*config.ServiceConfig, error) {
	cfg := new(config.ServiceConfig)
	err := toml.Unmarshal(configBytes, cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
