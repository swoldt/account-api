package helper

import (
	"io/ioutil"
)

func (h *Helper) BytesFromFile(path string) []byte {
	bytes, err := ioutil.ReadFile(path)

	h.suite.Require().NoError(err)

	return bytes
}

func (h *Helper) BytesToFile(path string, data []byte) {

	err := ioutil.WriteFile(path, data, 0644)
	h.suite.Require().NoError(err)
}