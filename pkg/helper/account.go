package helper

import (
	"github.com/google/uuid"
)

// GetMockAccount returns a mock account to test with
func (h *Helper) GetMockAccount() []byte {
	return []byte(`{
		"data": {
			"type": "accounts",
			"id": "` + uuid.New().String() + `",
			"organisation_id": "eb0bd6f5-c3f5-44b2-b677-acd23cdde73c",
			"version": 0,
			"attributes": {
				"country": "GB",
				"base_currency": "GBP",
				"account_number": "41426819",
				"bank_id": "400300",
				"bank_id_code": "GBDSC",
				"bic": "NWBKGB22",
				"iban": "GB11NWBK40030041426819",
				"status": "confirmed"
			}
		}
	}`)
}
