package domain

// Account basic structure
type Account struct {
	Type           string        `json:"type" validate:"required"`
	ID             string        `json:"id" validate:"required,uuid"`
	OrganisationID string        `json:"organisation_id" validate:"required,uuid"`
	Version        int           `json:"version" validate:"gte=0"`
	Attributes     Attributes    `json:"attributes" validate:"required"`
	Relationships  Relationships `json:"relationships"`
}

// Attributes of an Account
type Attributes struct {
	Country string `json:"country" validate:"required,oneof=GB AU BE CA FR DE GR HK IT LU NL PL PT ES CH US"`
	*OptionalAttributes
}

// OptionalAttributes can be empty
type OptionalAttributes struct {
	BaseCurrency  string `json:"base_currency,omitempty" validate:"oneof=GBP AUD EUR CAD HKD PLN CHF USD"`
	AccountNumber string `json:"account_number,omitempty" `
	BankID        string `json:"bank_id,omitempty" validate:"lte=11"`
	BankIDCode    string `json:"bank_id_code,omitempty" validate:"oneof=GBDSC AUBSB BE CACPA FR DEBLZ GRBIC HKNCC ITNCC LULUX PLKNR PTNCC ESNCC CHBCC USABA"`
	Bic           string `json:"bic,omitempty" validate:"gte=6,lte=8"`
	Iban          string `json:"iban,omitempty" validate:"gt=0"`
	Status        string `json:"status,omitempty" validate:"gt=0"`
}

// Relationships holds a slice of AccountEvents
type Relationships struct {
	AccountEvents struct {
		Data []AccountEvent `json:"data"`
	} `json:"account_events"`
}

// AccountEvent identifies the type of event and account related to it
type AccountEvent struct {
	Type string `json:"type" validate:"required,gt=0"`
	ID   string `json:"id" validate:"required,gt=0"`
}
