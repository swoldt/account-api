package domain

// ListAccountParams supports pagination
type ListAccountParams struct {
	Pagination AccountPagination
}

// AccountPagination takes a Number or page and a page size
type AccountPagination struct {
	Number int
	Size   int
}

// AccountDTO is the raw structure returned by Fetch/Create Account
type AccountDTO struct {
	Data Account `json:"data"`
}

// ListAccountDTO is the raw structure returned by ListAccounts
type ListAccountDTO struct {
	Data []Account `json:"data"`
}

// ResponseError indicates why the request failed
type ResponseError struct {
	Error string `json:"error_message"`
}
