package api

import (
	"bitbucket.org/swoldt/accounts-api/pkg/client"
	"bitbucket.org/swoldt/accounts-api/pkg/config"
	"bitbucket.org/swoldt/accounts-api/pkg/interfaces"
	"bitbucket.org/swoldt/pkg/xlogger"
	"github.com/go-playground/validator/v10"
)

// AccountAPI represents the struct for this package
type AccountAPI struct {
	config    *config.ServiceConfig
	log       *xlogger.Logger
	validator *validator.Validate
	cli       interfaces.APIClient
}

// New returns an initiliazed API service
func New(log *xlogger.Logger, cfg *config.ServiceConfig) interfaces.API {

	return &AccountAPI{
		log:       log,
		config:    cfg,
		validator: validator.New(),
		cli:       client.New(log, cfg),
	}
}
