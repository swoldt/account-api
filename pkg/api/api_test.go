package api_test

import (
	"bitbucket.org/swoldt/accounts-api/pkg/domain"
	"github.com/stretchr/testify/require"
)

func (s *TestSuite) Test_AccountAPI() {
	dto := domain.AccountDTO{}
	s.helper.FromJSON(s.helper.GetMockAccount(), &dto)

	s.createSuccess(dto)
	s.createFailure(dto)
	s.fetchSuccess(dto)
	s.deleteSuccess(dto)
	s.fetchFailure(dto)
	s.deleteFailure(dto)
}

func (s *TestSuite) Test_List_Accounts() {
	lenAccounts := 11
	s.createAccounts(lenAccounts)
	defer s.cleanUp()

	// test list account
	accounts, err := s.api.List(&domain.ListAccountParams{
		Pagination: domain.AccountPagination{
			Number: lenAccounts + 1,
			Size:   1,
		},
	})

	require.NoError(s.T(), err)
	require.Equal(s.T(), 0, len(accounts))

	// providing a nil param will result in all accounts being returned
	accounts, err = s.api.List(nil)

	require.NoError(s.T(), err)
	require.Equal(s.T(), lenAccounts, len(accounts))
}

func (s *TestSuite) createAccounts(n int) {
	for i := 0; i < n; i++ {
		dto := domain.AccountDTO{}
		s.helper.FromJSON(s.helper.GetMockAccount(), &dto)
		s.createSuccess(dto)
	}
}

func (s *TestSuite) createSuccess(dto domain.AccountDTO) {
	// test create account
	account, err := s.api.Create(dto.Data)

	require.NoError(s.T(), err)
	require.Equal(s.T(), dto.Data.ID, account.ID)
}

func (s *TestSuite) createFailure(dto domain.AccountDTO) {
	dto.Data.Attributes.Country = "does not exist"
	dto.Data.ID = ""

	account, err := s.api.Create(dto.Data)
	require.Error(s.T(), err)
	require.Equal(s.T(), account, (*domain.Account)(nil))
}

func (s *TestSuite) fetchSuccess(dto domain.AccountDTO) {
	// test fetch account
	account, err := s.api.Fetch(dto.Data.ID)
	require.NoError(s.T(), err)
	require.Equal(s.T(), dto.Data.ID, account.ID)
}

func (s *TestSuite) fetchFailure(dto domain.AccountDTO) {
	doesNotExist, err := s.api.Fetch(dto.Data.ID)
	require.Error(s.T(), err)
	require.Equal(s.T(), doesNotExist, (*domain.Account)(nil))
}

func (s *TestSuite) deleteSuccess(dto domain.AccountDTO) {
	// test delete account
	err := s.api.Delete(dto.Data.ID, dto.Data.Version)
	require.NoError(s.T(), err)
}

func (s *TestSuite) deleteFailure(dto domain.AccountDTO) {
	dto.Data.ID = ""
	// test delete account
	err := s.api.Delete(dto.Data.ID, dto.Data.Version)
	require.Error(s.T(), err)
}

func (s *TestSuite) cleanUp() {
	accounts, err := s.api.List(nil)
	require.NoError(s.T(), err)

	for _, acc := range accounts {
		err = s.api.Delete(acc.ID, acc.Version)
		require.NoError(s.T(), err)
	}
}
