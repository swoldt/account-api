package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/swoldt/accounts-api/pkg/domain"
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
)

// List returns an Account slice
func (api *AccountAPI) List(params *domain.ListAccountParams) ([]domain.Account, error) {
	dto := &domain.ListAccountDTO{}

	path := api.config.Form3AccountsRoute

	// when pagination is desired params will be added to the request path
	if params != nil {
		path = fmt.Sprintf("%s?page[number]=%d&page[size]=%d",
			api.config.Form3AccountsRoute,
			params.Pagination.Number,
			params.Pagination.Size)
	}

	err := api.cli.Do("GET", path, nil, dto, http.StatusOK)
	if err != nil {
		return nil, err
	}

	return dto.Data, nil
}

// Create (s) an Account
func (api *AccountAPI) Create(account domain.Account) (*domain.Account, error) {

	err := api.validate(&account)
	if err != nil {
		return nil, errors.Wrap(err, "validate new account failed")
	}

	dto := &domain.AccountDTO{
		Data: account,
	}

	b, err := json.Marshal(dto)
	if err != nil {
		return nil, errors.Wrap(err, "marshal new account failed")
	}

	dto = &domain.AccountDTO{}

	err = api.cli.Do("POST", api.config.Form3AccountsRoute, bytes.NewReader(b), dto, http.StatusCreated)
	if err != nil {
		return nil, err
	}

	return &dto.Data, nil
}

// validate if account properties on create event are as expected
func (api *AccountAPI) validate(account *domain.Account) error {

	err := api.validator.Struct(account)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			api.log.Errorf("validation for account %s condition of %s %s %s not met with %+v", err.Namespace(), err.Kind(), err.Tag(), err.Param(), account)
		}
	}

	return err
}

// Fetch finds an account by id
func (api *AccountAPI) Fetch(id string) (*domain.Account, error) {

	path := fmt.Sprintf("%s/%s", api.config.Form3AccountsRoute, id)

	dto := &domain.AccountDTO{}

	err := api.cli.Do("GET", path, nil, dto, http.StatusOK)
	if err != nil {
		return nil, err
	}

	return &dto.Data, nil
}

// Delete (s) an account by id and version
func (api *AccountAPI) Delete(id string, version int) error {

	path := fmt.Sprintf("%s/%s?version=%d", api.config.Form3AccountsRoute, id, version)

	err := api.cli.Do("DELETE", path, nil, nil, http.StatusNoContent)
	if err != nil {
		return err
	}

	return nil
}
