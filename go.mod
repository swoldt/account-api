module bitbucket.org/swoldt/accounts-api

go 1.13

require (
	bitbucket.org/swoldt/pkg v0.0.0-20191129123942-7dd98be38b1a
	github.com/BurntSushi/toml v0.3.1
	github.com/go-playground/validator v9.31.0+incompatible // indirect
	github.com/go-playground/validator/v10 v10.2.0
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/google/uuid v1.1.1
	github.com/imdario/mergo v0.3.9 // indirect
	github.com/namsral/flag v1.7.4-pre // indirect
	github.com/pelletier/go-toml v1.7.0
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sirupsen/logrus v1.5.0 // indirect
	github.com/stretchr/testify v1.4.0
	gopkg.in/dealancer/validate.v2 v2.1.0 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/h2non/gock.v1 v1.0.15
)
