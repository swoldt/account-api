test:
	go test ./pkg/api/. -coverprofile coverage/api.out 
	go test ./pkg/client/. -coverprofile coverage/client.out

gc: 
	go tool cover -html=coverage/api.out 
	go tool cover -html=coverage/client.out
 
# Serve task will run fswatch monitor and performs restart task if any source file changed. Before serving it will execute start task.
serve: test
	fswatch -or --event=Updated ./pkg | xargs -n1 -I {} make test
  
# .PHONY is used for reserving tasks words
.PHONY: test serve gc